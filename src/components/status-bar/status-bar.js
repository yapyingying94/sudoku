import React from 'react';

import TimerWithPause from '../timer-with-pause/timer-with-pause';
import MenuButton from '../menu-button/menu-button';
import SettingsButton from '../settings-button/settings-button';
import HintButton from '../hint-button/hint-button';
import FullscreenButton from '../fullscreen-button/fullscreen-button';


import './status-bar.css';

const stopPropagation = (e) => e.stopPropagation();


function SiteLink () {
    return (
        <div style={{textAlign: "center", height: "7vh", lineHeight: "7vh"}}>
            <div style={{ display:"inline"}}>
                <img src="logo-source.svg" style={{width: "3%", position: "relative", top: "0.5vh", paddingRight: "20px"}} alt=""/>
            </div>
            <div style={{display:"inline"}}>Rapid Sudoku!</div>
                
        </div>
    );
}


function StatusBar ({
    showTimer, startTime, intervalStartTime, endTime, pausedAt, hintsUsedCount,
    showPencilmarks, menuHandler, pauseHandler, initialDigits
}) {
    const timer = showTimer
        ? (
            <TimerWithPause
                startTime={startTime}
                intervalStartTime={intervalStartTime}
                endTime={endTime}
                pausedAt={pausedAt}
                pauseHandler={pauseHandler}
                hintsUsedCount={hintsUsedCount}
            />
        )
        : null;
    return (
        <div className="status-bar" onMouseDown={stopPropagation}>
            <SiteLink />
            {timer}
            <div className="status-bar-buttons">
                <FullscreenButton />
                <HintButton menuHandler={menuHandler} />
                <SettingsButton menuHandler={menuHandler} />
                <MenuButton
                    initialDigits={initialDigits}
                    showPencilmarks={showPencilmarks}
                    menuHandler={menuHandler}
                />
            </div>
        </div>
    );
}

export default StatusBar;
